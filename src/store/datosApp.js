import { defineStore } from "pinia";
import { reactive, ref } from "vue";
import { read, utils } from 'xlsx';
export const useDatosStore = defineStore('datos', () => {
    const isLogin = ref(false)
    const regalos = reactive([
        { id: 1, imagen: 'canasta.png', nombre: 'Polo', distritos: [] },
        { id: 2, imagen: 'plancha.png', nombre: 'Canasta', distritos: [] },
        { id: 3, imagen: 'sonido.webp', nombre: 'Sorpresa', distritos: [] },
        { id: 4, imagen: 'licuadora.png', nombre: 'Casaca', distritos: [] },
        /*{ id: 5, imagen: 'microondas.png', nombre: 'Horno Microondas', distritos: [] },
        { id: 6, imagen: 'olla.png', nombre: 'Olla Arrocera', distritos: [] },
        { id: 7, imagen: 'tv.png', nombre: 'TV', distritos: [] },
        { id: 8, imagen: 'freidora.png', nombre: 'Freidora de Aire', distritos: [] },
        { id: 9, imagen: 'refrigeradora.png', nombre: 'Refrigeradora', distritos: [] },*/
    ])
    const clientes = reactive([])
    function isLoginOk() {
        isLogin.value = true
    }
    function logout() {
        isLogin.value = false
    }
    function loadClientes(file) {
        //const file = fileInput.value.files[0];
        const reader = new FileReader();

        reader.onload = (e) => {
            const wb = read(e.target.result);
            /* generate array of objects from first worksheet */
            const ws = wb.Sheets[wb.SheetNames[0]]; // get the first worksheet
            const data = utils.sheet_to_json(ws);
            //
            let iddistrito = 100
            //
            const distritos = {};
            data.forEach(item => {
                const { DIRECCION, DISTRITO, NOMBRES, SUMINISTRO } = item;
                if (!distritos[DISTRITO]) {
                    distritos[DISTRITO] = {
                        id: iddistrito,
                        nombre: DISTRITO,
                        activo: false,
                        clientes: []
                    };
                    regalos.forEach((item) => {
                        let distritor = {
                            id: iddistrito,
                            nombre: DISTRITO,
                            cantidad: 0,
                            activo: false,
                            ganadores: []
                        }
                        item.distritos.push(distritor)
                    })

                    iddistrito += 1
                }
                const distrito = distritos[DISTRITO];
                let cliente = {
                    id: SUMINISTRO,
                    nombres: NOMBRES,
                    direccion: DIRECCION,
                    distrito: DISTRITO
                };
                distrito.clientes.push(cliente);
            })
            let distritoTotal = {
                id: iddistrito,
                nombre: 'GENERAL',
                activo: false,
                clientes: []
            }
            regalos.forEach((item) => {
                let distritor = {
                    id: iddistrito,
                    nombre: 'GENERAL',
                    cantidad: 0,
                    ganadores: []
                }
                item.distritos.push(distritor)
            })
            data.forEach(item => {
                const { DIRECCION, DISTRITO, NOMBRES, SUMINISTRO } = item;
                let cliente = {
                    id: SUMINISTRO,
                    nombres: NOMBRES,
                    direccion: DIRECCION,
                    distrito: DISTRITO
                };
                distritoTotal.clientes.push(cliente)
            })
            distritos['GENERAL'] = distritoTotal
            clientes.splice(0, clientes.length, ...Object.values(distritos));
            //Obtener los diferentes
        }
        reader.readAsArrayBuffer(file);
        console.log(regalos)

    }

    function eliminaCliente(id){
        let numero=clientes.map(c=>c.id).indexOf(id)
        clientes.splice(numero,1)
    }

    return { isLogin, isLoginOk, logout, regalos, loadClientes, clientes , eliminaCliente}
})