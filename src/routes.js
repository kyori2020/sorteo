import Dashboard from './views/Dashboard.vue';
import App from './App.vue';

import Login from './views/Login.vue';
import Sorteo from './views/Sorteo.vue';
import Configurar from './views/Configurar.vue';

const routes = [
    { path: '/', component: Login },
    { path: '/dashboard', component: Dashboard, dmeta: { requiresAuth: true }},
    { path: '/configurar', component: Configurar, meta: { requiresAuth: true } },
    { path: '/sorteo', component: Sorteo, meta: { requiresAuth: true }},
];
export default routes;