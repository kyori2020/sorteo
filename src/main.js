import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router';
import routes from './routes';
import { createPinia } from 'pinia'
import { useDatosStore } from './store/datosApp';

const pinia = createPinia()
const authStore = useDatosStore(pinia)
const router = createRouter({
    history: createWebHistory(),
    routes,
  });
  
  router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (!authStore.isLogin) {
        next('/');
      } else {
        next();
      }
    } else {
      next();
    }
  });


createApp(App).use(pinia).use(router).mount('#app')
